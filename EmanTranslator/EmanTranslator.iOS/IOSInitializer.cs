﻿using System;
using System.IO;
using SQLite.Net.Interop;
using SQLite.Net.Platform.XamarinIOS;

namespace EmanTranslator.iOS
{
	public class IOSInitializer : ISQLInitializer
	{
		string _dbPath;
		SQLitePlatformIOS _platform;

		public IOSInitializer()
		{
		}

		public string DatabasePath
		{
			get
			{
				if (String.IsNullOrEmpty(_dbPath))
				{
					_dbPath = GetDatabasePath();
				}

				return _dbPath;
			}
		}

		public ISQLitePlatform Platform
		{
			get
			{
				if (_platform == null)
				{
					_platform = new SQLitePlatformIOS();
				}

				return _platform;
			}
		}

		public string GetDatabasePath()
		{
			string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
			string libraryPath = Path.Combine(documentsPath, "..", "Library"); // Library folder
			var path = Path.Combine(libraryPath, "EMANDBLite.db3");

			return path;
		}
	}
}

