using Foundation;
using System;
using UIKit;
using MvvmCross.iOS.Views;
using EmanTranslator.ViewModels;
using MvvmCross.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using EmanTranslator.Model;
using EmanTranslator.iOS.Convertors;

namespace EmanTranslator.iOS
{
	[MvxFromStoryboard("Main")]
    public partial class TranslatorView : MvxTableViewController
    {
		public TranslatorViewModel TranslatorViewModel
		{
			get
			{
				return (TranslatorViewModel)ViewModel;
			}
		}

        public TranslatorView (IntPtr handle) : base (handle)
        {
        }

		public override void ViewDidLoad()
		{
			this.Request = new MvxViewModelRequest<TranslatorViewModel>(null, null, new MvxRequestedBy());

			base.ViewDidLoad();

			TranslatorViewModel.OnTranslatorActiveChange += TranslatorViewModel_OnTranslatorActiveChange;

			var source = new TranslatorSource(this);

			var set = this.CreateBindingSet<TranslatorView, TranslatorViewModel>();
			set.Bind(source).To(vm => vm.Records);

			set.Bind(btnTranslate).To(vm => vm.TranslateCommand);

			set.Bind(tfLng).To(vm => vm.Longitude).WithConversion(nameof(StringToDoubleConvertor));
			set.Bind(tfLat).To(vm => vm.Latitude).WithConversion(nameof(StringToDoubleConvertor)); 

			set.Apply();

			TableView.Source = source;
		}

		void TranslatorViewModel_OnTranslatorActiveChange(object sender, bool isActive)
		{
			btnTranslate.Enabled = tfLat.Enabled = tfLng.Enabled = !isActive;

			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = isActive;
		}

		class TranslatorSource : MvxTableViewSource
		{
			readonly NSString cellId = new NSString("TranslatorCell");

			public TranslatorSource(TranslatorView owner)
				: base (owner.TableView)
			{
			}

			protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
			{
				var cell = tableView.DequeueReusableCell(cellId, indexPath);

				var record = (TranslatorRecord)item;

				cell.TextLabel.Text = record.Location;
				cell.DetailTextLabel.Text = record.CreatedDate.LocalDateTime.ToString("g");

				return cell;
			}
		}
	}
}