// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace EmanTranslator.iOS
{
	[Register ("TranslatorView")]
	partial class TranslatorView
	{
		[Outlet]
		UIKit.UIButton btnTranslate { get; set; }

		[Outlet]
		UIKit.UITextField tfLat { get; set; }

		[Outlet]
		UIKit.UITextField tfLng { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnTranslate != null) {
				btnTranslate.Dispose ();
				btnTranslate = null;
			}

			if (tfLat != null) {
				tfLat.Dispose ();
				tfLat = null;
			}

			if (tfLng != null) {
				tfLng.Dispose ();
				tfLng = null;
			}
		}
	}
}
