using System.Reflection;
using EmanTranslator.Queries;
using EmanTranslator.Repositories.Typed;
using EmanTranslator.Services;
using MvvmCross.Platform;
using MvvmCross.Platform.IoC;

namespace EmanTranslator
{
    public class App : MvvmCross.Core.ViewModels.MvxApplication
    {
        public override void Initialize()
        {
			typeof(ISQLConnectionService).GetTypeInfo().Assembly.CreatableTypes()
				.AsInterfaces()
				.RegisterAsLazySingleton();

			typeof(TranslatorRecordQuery).GetTypeInfo().Assembly.CreatableTypes()
				.EndingWith("Query")
				.AsTypes()
				.RegisterAsDynamic();

			typeof(TranslatorRecordRepository).GetTypeInfo().Assembly.CreatableTypes()
				.EndingWith("Repository")
				.AsTypes()
				.RegisterAsDynamic();
			
			RegisterAppStart<ViewModels.TranslatorViewModel>();
        }
    }
}
