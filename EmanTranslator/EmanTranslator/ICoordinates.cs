﻿using System;

namespace EmanTranslator
{
	public interface ICoordinates
	{
		double Latitude { get; set; }

		double Longitude { get; set; }

        string Location { get; set; }
    }
}

