﻿using SQLite.Net.Interop;

namespace EmanTranslator
{
	public interface ISQLInitializer
	{
		string DatabasePath { get; }

		ISQLitePlatform Platform { get; }
	}
}