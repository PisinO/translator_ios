﻿using System;
using SQLite.Net.Attributes;

namespace EmanTranslator.Model
{
	public class TranslatorRecord : IEntity<Guid>, ICoordinates
	{
		/// <summary>
		/// Gets or sets the identifier.
		/// </summary>
		/// <value>The identifier.</value>
		[PrimaryKey]
		public Guid Id { get; set; }

		/// <summary>
		/// Gets or sets the created date.
		/// </summary>
		/// <value>The created date.</value>
		public DateTimeOffset CreatedDate { get; set; }

		#region ICoordinates implementation

		public double Latitude { get; set; }

		public double Longitude { get; set; }

		public string Location { get; set; }

		#endregion

		public TranslatorRecord()
		{
			Id = Guid.NewGuid();
			CreatedDate = DateTimeOffset.UtcNow;
		}
	}
}

