﻿using System.Collections.Generic;

namespace EmanTranslator.Queries
{
    public interface IQuery<TResult>
    {
        IList<TResult> Execute();
    }
}
