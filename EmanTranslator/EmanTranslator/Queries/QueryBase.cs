using System;
using System.Collections.Generic;
using System.Linq;
using EmanTranslator.Services;
using SQLite.Net;

namespace EmanTranslator.Queries
{
	public abstract class QueryBase<TResult> : IQuery<TResult>
	{
		protected SQLiteConnection Connection {
			get {
				return connection.Connection;
			}
		}

		readonly ISQLConnectionService connection;

		protected QueryBase(ISQLConnectionService connection)
		{
			this.connection = connection;
		}

        public int? Skip { get; set; }

        public int? Take { get; set; }


		protected abstract TableQuery<TResult> GetQueryable();

        public IList<TResult> Execute()
        {
            var query = GetQueryable();

            if (Skip != null)
            {
                query = query.Skip(Skip.Value);
            }
            if (Take != null)
            {
                query = query.Take(Take.Value);
            }

			var results = query.ToList();

            PostProcessResults(results);

            return results;
        }

        protected virtual void PostProcessResults(List<TResult> results)
        {
        }

        public int GetTotalRowCount()
        {
            return GetQueryable().Count();
        }
    }
}