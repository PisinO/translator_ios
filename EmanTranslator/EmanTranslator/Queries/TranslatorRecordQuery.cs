﻿using System;
using EmanTranslator.Model;
using EmanTranslator.Services;
using SQLite.Net;

namespace EmanTranslator.Queries
{
	public class TranslatorRecordQuery : QueryBase<TranslatorRecord>
	{
		public TranslatorRecordQuery(ISQLConnectionService connection)
			: base (connection)
		{
			Take = 10;
		}

		protected override TableQuery<TranslatorRecord> GetQueryable()
		{
			TableQuery<TranslatorRecord> records = Connection.Table<TranslatorRecord>();

			records = records.OrderByDescending(x => x.CreatedDate);

			return records;
		}
	}
}

