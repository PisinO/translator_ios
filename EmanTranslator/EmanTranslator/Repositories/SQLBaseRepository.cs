﻿using System;
using System.Diagnostics;
using EmanTranslator.Services;

namespace EmanTranslator.Repositories
{
	public class SQLBaseRepository<TEntity, TKey> where TEntity : class, IEntity<TKey>
	{
		protected readonly ISQLConnectionService connectionService;

		public SQLBaseRepository(ISQLConnectionService connectionService)
		{
			this.connectionService = connectionService;
		}

		public void InsertOrReplace(TEntity item)
		{
			try
			{
				connectionService.Connection.InsertOrReplace(item);
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}
		}
	}
}