﻿using System;
using EmanTranslator.Model;
using EmanTranslator.Services;

namespace EmanTranslator.Repositories.Typed
{
	public class TranslatorRecordRepository : SQLBaseRepository<TranslatorRecord, Guid>
	{
		public TranslatorRecordRepository(ISQLConnectionService connectionService)
			: base (connectionService)
		{
		}
	}
}

