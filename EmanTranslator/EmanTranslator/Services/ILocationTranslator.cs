﻿using System;
using System.Threading.Tasks;

namespace EmanTranslator.Services
{
	public interface ILocationTranslator
	{
		Task<string> Translate (ICoordinates coordinates);
    }
}