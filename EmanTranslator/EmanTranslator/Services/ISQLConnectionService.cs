﻿using System;
using SQLite.Net;

namespace EmanTranslator.Services
{
	public interface ISQLConnectionService : IDisposable
	{
		SQLiteConnection Connection { get; }
	}
}

