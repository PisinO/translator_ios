﻿using System;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;
using System.Linq;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Diagnostics;
using EmanTranslator.Model;

namespace EmanTranslator.Services
{
	public class BaseClient
	{
		const string baseUrl = "http://maps.googleapis.com/maps/api/geocode/json";

		private HttpClient _client;

		protected HttpClient Client {
			get { 
				if (_client == null) {
					InitHttpClient ();
				}

				return _client;
			}
		}

		private void InitHttpClient()
		{
			_client = new HttpClient();		    

			// Base url
			_client.BaseAddress = new Uri(baseUrl);

			_client.DefaultRequestHeaders.Accept.Add (new MediaTypeWithQualityHeaderValue ("application/json"));
		}

		protected void DisposeClient ()
		{
			_client.Dispose ();
			_client = null;
		}
	}

	public class LocationTranslator : BaseClient, ILocationTranslator
	{
		NumberFormatInfo _nfi;

		public LocationTranslator ()
		{
			_nfi = new NumberFormatInfo();
			_nfi.NumberDecimalSeparator = ".";
		}

		async Task<GoogleMapsRootObject> CallGoogleApi (ICoordinates coordinates)
		{
			if (coordinates.Latitude == 0 || coordinates.Longitude == 0) {
				throw new ArgumentException ("coordinates.Latitude and coordinates.Longitude can't be zero.");
			}

			string url = $"?latlng={coordinates.Latitude.ToString(_nfi)},{coordinates.Longitude.ToString(_nfi)}";

			HttpResponseMessage apiResponse = null;
			GoogleMapsRootObject response = null;

			try {
				apiResponse = await Client.GetAsync (url);

				if (apiResponse.StatusCode != HttpStatusCode.OK) {
					throw new Exception ("Response error");
				}

				string stringResponse = await apiResponse.Content.ReadAsStringAsync();

				System.Diagnostics.Debug.WriteLine ($"Google translator response: {stringResponse}");

				response = JsonConvert.DeserializeObject<GoogleMapsRootObject>(stringResponse);
			} catch (Exception ex) {
				Debug.WriteLine(ex.Message);
			} finally {
				DisposeClient ();
			}

			return response;
		}

		#region ILocationTranslator implementation

		public async Task<string> Translate (ICoordinates coordinates)
		{
			GoogleMapsRootObject response = null;

			try {
				response = await CallGoogleApi (coordinates);
			} catch (Exception ex) {
				Debug.WriteLine (ex.Message);
			}

			if (response == null || response.results == null || !response.results.Any()) {
				return String.Empty;
			}

			var locationPoint = response.results?.FirstOrDefault(); // First is the most accurate

			string output = "";

			// Returns the nearest city - Praha, Brno, etc...
			output = locationPoint?.address_components?
			                       .FirstOrDefault(x => x.types.Contains("locality") && x.types.Contains("political"))?
			                       .long_name;

			// otherwise returns the most accurate point
			if (String.IsNullOrEmpty(output))
			{
				output = locationPoint?.address_components?
									   .FirstOrDefault().long_name;
			}

			return output;
		}

        #endregion
    }
}