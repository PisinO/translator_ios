﻿using System;
using EmanTranslator.Model;
using SQLite.Net;

namespace EmanTranslator.Services
{
	public class SQLConnectionService : ISQLConnectionService
	{
		readonly Type[] _entities = {
			typeof(TranslatorRecord)
		};

		readonly ISQLInitializer _initializer;
		private SQLiteConnection _con;

		public SQLConnectionService(ISQLInitializer initializer)
		{
			_initializer = initializer;
		}

		public SQLiteConnection Connection
		{
			get
			{
				if (_con == null)
				{
					_con = new SQLiteConnection(_initializer.Platform, _initializer.DatabasePath);
					InitTables();
				}

				return _con;
			}
		}

		void InitTables()
		{
			foreach (var type in _entities)
			{
				Connection.CreateTable(type);
			}
		}

		public void Dispose()
		{
			_con.Close();
			_con.Dispose();
			_con = null;
		}
	}
}

