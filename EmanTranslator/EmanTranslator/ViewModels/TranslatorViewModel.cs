﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using EmanTranslator.Model;
using EmanTranslator.Queries;
using EmanTranslator.Repositories.Typed;
using EmanTranslator.Services;
using MvvmCross.Core.ViewModels;

namespace EmanTranslator.ViewModels
{
	public class TranslatorViewModel : MvxViewModel, ICoordinates
	{
		#region ICoordinates implementation

		public double Latitude { get; set; } = 50.0480928;
		public double Longitude { get; set; } = 14.4574355;
		public string Location { get; set; }

		#endregion

		public ObservableCollection<TranslatorRecord> Records { get; set; }
		public IMvxCommand TranslateCommand { get; set; }

		public event EventHandler<bool> OnTranslatorActiveChange;

		readonly TranslatorRecordQuery _translatorQuery;
		readonly ILocationTranslator _locationTranslator;
		readonly TranslatorRecordRepository _translatorRecordRepository;

		public TranslatorViewModel(TranslatorRecordQuery translatorQuery,
		                           ILocationTranslator locationTranslator,
		                           TranslatorRecordRepository translatorRecordRepository)
		{
			_translatorQuery = translatorQuery;
			_locationTranslator = locationTranslator;
			_translatorRecordRepository = translatorRecordRepository;

			Records = new ObservableCollection<TranslatorRecord>();

			TranslateCommand = new MvxCommand(async () => {
				OnTranslatorActiveChange(this, true);

				try
				{
					var newRecord = new TranslatorRecord();

					newRecord.Location = await _locationTranslator.Translate(this);

					// Save
					_translatorRecordRepository.InsertOrReplace(newRecord);
				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.Message);
				}
				finally
				{
					ReloadRecords();

					OnTranslatorActiveChange(this, false);
				}
			});
		}

		public void Init()
		{
			ReloadRecords();
		}

		// Zatím se natvrdo smaže a znovu naplní celá kolekce ...
		private void ReloadRecords()
		{
			Records.Clear();

			foreach (var record in _translatorQuery.Execute())
			{
				Records.Add(record);
			}
		}
	}
}

